.PHONY: dev format test docs clean

dev:
	python -m venv .venv
	. .venv/bin/activate; pip install -U pip
	. .venv/bin/activate; pip install -e .[doc]

format:
	black -l 120 .
	black --check -l 120 .

test:
	python -B -m unittest discover -s tests -p test_*.py

docs:
	sphinx-build -b html docs public

clean:
	-rm -r .venv *.egg-info build dist docs/dist coverage .coverage pip-wheel-metadata */__pycache__ */*/__pycache__
