Telegen
#######

.. code::

    usage: telegen [-h] [--overwrite] language [src]

    A CLI tool that generates code by parsing the Telegram Bot API page

    positional arguments:
    language         Output language
    src              Source file (will be fetched if not passed)

    optional arguments:
    -h, --help       show this help message and exit
    --overwrite, -o  Delete conflicting files before emitting (default: exit)


Docs
==========

https://strychnide.gitlab.io/telegen
