Extending Telegen
=================

This page explains some design decisions behind telegen and how to extend it to generate definitions in another language

Parser
------

The parser is a class with only one function: parse. Parse receives the HTML source as a string as the only argument and returns a tuple (version, endpoints, types, reverse_aliases):

- version: the telegram API version described by the fetched page
- endpoints: a list of endpoints that will be translated into functions
- types: a list of types that will be translated into structs/objects/whatever; note that the parser will keep only the types that are actually used
- reverse_aliases: a mapping between the "implementation" to the "abstract class" (InlineQueryResultAudio -> InlineQueryResult)

Endpoints and types
^^^^^^^^^^^^^^^^^^^

- endpoints and types share the same representation: a tuple (name, (param1, param2, ..., paramN), docstring)
- each parameter is a tuple (name, (type1, type2, ..., typeN), required ? True : False, docstring)
- each type is a tuple (base_type, composite_type1, composite_type2, composite_typeN), e.g. ("String", "Array", "Array") means List[List[String]] in Python terms

Note that this type representation is extremely limiting and can't express a type like List[Union[str int]]. However there is only one type with this requirement in the Telegram API, and that is "Array of InputMediaPhoto and InputMediaVideo": in this case the type will be parsed as ("InputMediaPhoto and InputMediaVideo", "Array"). I'm implementing the two methods separately for the sake of simplicity, but if this becomes relevant I'm accepting suggestions.

Also note that "Array" is the only composite_type and it's very likely that it's going to stay this way (maps are modeled as objects), so you can just wrap your base_type in a number of arrays/lists equal to "len(tuple) - 1"

Aliases
^^^^^^^

Some params admit a type that acts as an abstract class: the type itself isn't defined, but there are many implementations. In this case you can either
- substitute the type with the OR'ed aliases (e.g. InputMedia -> InputMediaPhoto or InputMediaVideo or ...)
- use the type and declare it as an abstract class and implement the derivated types as child classes.

The ``reversed_aliases`` dict is used to find the parent class: if a type is a key of ``reversed_aliases`` the associated value is the base class.

Generators
----------

| Each generator is a class with a single function: generate. Generate receives four arguments: version, endpoints, types, reverse_aliases and returns a dict {filename: content}.
| The flow to generate the code should be roughly as follows:

1. iterate over the endpoints and output the function definitions, keeping in mind that:

    - each function should take as arguments only the given params (and at most this/self)
    - each function should output the {language}-equivalent of a tuple (method, endpoint, headers, params, body)
    - some functions can be implemented by hand, to give the user a better API (see "Adding a generator" below)

2. iterate over the types and output the definitions, keeping in mind that:

    - types can be objects/maps/dicts or anything you can easily serialize (but they **must** be typed if the language allows it)
    - some types may be implemented by hand, to give the user a better API i.e. InputFile or ReplyMarkup types (see "Adding a generator" below)
    - some types are children of basic classes (if reverse_aliases[typename] returns something that's the basic class)

3. format the output with an opinionated tool i.e. black for python using the default settings


Adding a generator (example with javascript)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. create the file telegen/generators/javascript.py
2. inside the file define a class JavascriptApiGenerator with the ``generate`` method (see above)
3. create a telegen/includes/javascript directory: it will serve as a base for the generated package (and you can put everything you need here, i.e. tests)
4. add the new class to the dictionary inside telegen/__init__.py (should be self explanatory)
5. add some tests, we don't aim for 100% coverage but test at least the basic sendMessage, sendPhoto and any handwritten method
