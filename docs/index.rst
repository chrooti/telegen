Welcome to telegen's documentation!
===================================

.. code-block:: none

    usage: telegen [-h] [--overwrite] language [src]

    A CLI tool that generates code by parsing the Telegram Bot API page

    positional arguments:
    language         Output language
    src              Source file (will be fetched if not passed)

    optional arguments:
    -h, --help       show this help message and exit
    --overwrite, -o  Delete conflicting files before emitting (default: exit)

.. toctree::
   :maxdepth: 2
   :caption: Table of Contents

   quickstart
   extending
