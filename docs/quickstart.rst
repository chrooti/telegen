Quickstart
===========

This page gives you an idea of how the flow goes for working with telegen. If you don't want to generate the definitions yourself you're better off using the telegen_language package for your programming language of choice.

First off install telegen

.. code-block:: none

    pip install telegen

Then generate the definitions and install them:

.. code-block:: none

    telegen python
    cd telegen_python
    pip install .

Now create a file with the following content:

.. code-block:: python
    :caption: example.py

    import json
    from urllib.request import Request, urlencode, urlopen

    from .telegen_python import sendMessage

    token = "insert_your_token"

    # get the http parameters from the method
    method, endpoint, headers, params, body = sendMessage(chat_id, 'Hello world!')

    # call the api
    url = f"https://api.telegram.org/bot{token}/{endpoint}?{urlencode(params)}"
    req = Request(url, body, headers or {}, method=method)
    res = urlopen(req).read()

    # decode the json and read the response to check if it's okay
    res = json.loads(res.decode())
    assert res['ok'] == True


| If you run python -m folder_name from outside the folder you should now see the response from Telegram and a message sent from your bot to you!
| If you want to see some more advanced examples run telegen for your language and check the tests/ folder inside (or you can just browse telegen/includes/{language}/tests).
