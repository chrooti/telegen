from os.path import abspath, dirname
from unittest import TestCase
from urllib.request import urlopen

from telegen.generators.python import PythonApiGenerator
from telegen.parser import ApiParser


class GeneratorTest(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.doc_url = "https://core.telegram.org/bots/api"

    def test_parse_cache(self):
        current_dir = dirname(abspath(__file__))
        with open(f"{current_dir}/page_cache.txt", "r") as f:
            parser = ApiParser()
            parser.parse(f)

    def test_parse_fresh(self):
        doc = urlopen(self.doc_url)

        parser = ApiParser()
        parser.parse(doc)

    def test_generate_python(self):
        doc = urlopen(self.doc_url)

        parser = ApiParser()
        api_version, endpoints, types, reversed_aliases = parser.parse(doc)

        generator = PythonApiGenerator()
        generator.generate(api_version, endpoints, types, reversed_aliases)
